﻿<?php
	class Database {
		private $dbname;
		private $dbuser;
		private $dbpass;
		private $dbhost;
		private $pdo;

		public function __construct() {
			$this->dbname='complyMasterDb';
			$this->dbuser=$_SERVER['SERVER_NAME']=='localhost' ? 'root' : 'invd-admin-db';
			$this->dbpass=$_SERVER['SERVER_NAME']=='localhost' ? 'ok2login!' : 'e){=[7rKx#.4/hA>';
			$this->dbhost=$_SERVER['SERVER_NAME']=='localhost' ? 'localhost' : 'localhost';
			$this->pdo = null;
			try {
			 $this->pdo = new PDO("mysql:host=$this->dbhost;dbname=$this->dbname;charset=utf8", $this->dbuser, $this->dbpass);
			 $this->pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
			 $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}catch(PDOException $e) {
			 echo "Error : ". $e->getMessage();
			}
		}
		public function fetchAll($query,$values) {
			$stmt = $this->pdo->prepare($query);
			$stmt->execute($values);
			$rowCount = $stmt->rowCount();
			if ($rowCount <= 0) return array();
			else return $stmt->fetchAll();
		}
		public function fetchOne($query, $values) {
			$stmt = $this->pdo->prepare($query);
			$stmt->execute($values);
			$rowCount = $stmt->rowCount();
			if ($rowCount <= 0) return array();
			else return $stmt->fetch();
		}
		public function fetchAuthenticate($query,$username,$password) {
			$stmt = $this->pdo->prepare($query);
			$stmt->execute([$username,$password]);
			$rowCount = $stmt->rowCount();
			if ($rowCount <= 0) return array();
			else return $stmt->fetch();
		}
		public function insertToken($query, $user_id, $token, $date_created, $date_expired) {
			$stmt = $this->pdo->prepare($query);
			$stmt->execute([$user_id, $token, $date_created, $date_expired]);
		}
		public function updateOne($query, $values) {
			$stmt = $this->pdo->prepare($query);
			if($stmt->execute($values)) return true;
			else return false;
		}
		public function insertOne($query, $values) {
			$stmt = $this->pdo->prepare($query);
			if($stmt->execute($values)) return $this->pdo->lastInsertId();
			else return 0;
		}
		public function deleteOne($query, $values) {
			$stmt = $this->pdo->prepare($query);
			return $stmt->execute($values);
		}
		public function encrypt($string) {
			$edkey='Encrypted secret key for ComplyMaster';
			$output = false;
			$iv = md5(md5($edkey));
			$output = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($edkey), $string, MCRYPT_MODE_CBC, $iv);
			$output = base64_encode($output);
			return $output;
		}
		public function decrypt($string) {
			$edkey='Encrypted secret key for ComplyMaster';
			$output = false;
			$iv = md5(md5($edkey));
		    $output = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($edkey), base64_decode($string), MCRYPT_MODE_CBC, $iv);
		    $output = rtrim($output, "");
			return $output;
		}
	}
?>
