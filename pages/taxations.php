<?php
  session_name("ComplyMaster");
  session_start();
  if((isset($_SESSION["tout"]))&&($_SESSION["tout"]>time())) {
    $pTime = time();
    $expTime = $pTime + 600;
    $_SESSION["tout"] = $expTime;
    header ("Expires: ".gmdate("D, d M Y H:i:s", time())." GMT");
    header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header ("Cache-Control: no-cache, must-revalidate");
    header ("Pragma: no-cache");
    $title = 'Taxations';
    include_once "pages/header.php";
    require_once "config.php";
    require_once "DAO/taxations.php";
    require_once "DAO/taxCategories.php";
    $db = new Database();
    $taxations = new Taxations($db);
    $taxCategories = new TaxCategories($db);
    if(isset($_POST['btnAddTaxation'])) {
      $insertParams = array();
      if(strlen($_POST['txtName'])>0) $insertParams['name'] = str_replace('"',"`",str_replace("'","`",$_POST['txtName']));
      if(strlen($_POST['txtDate'])>0) $insertParams['date'] = str_replace('"',"`",str_replace("'","`",$_POST['txtDate']));
      if(strlen($_POST['txtCategory'])>0) $insertParams['category'] = str_replace('"',"`",str_replace("'","`",$_POST['txtCategory']));
      if(strlen($_POST['txtDescription'])>0) $insertParams['description'] = str_replace('"',"`",str_replace("'","`",$_POST['txtDescription']));
			$taxationsLastId=$taxations->insertTaxation($insertParams);
			if($taxationsLastId>0){
        echo "<div class='alert alert-success' role='alert'>
          <strong><span class='glyphicon glyphicon-ok'></span> Success!!!</strong> Record has been added successfully.
          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>
          </button>
        </div>";
      }else{
        echo "<div class='alert alert-danger' role='alert'>
          <strong><span class='glyphicon glyphicon-warning-sign'></span> Warning!!!</strong> An error has occurred. Please try again later.
          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>
          </button>
        </div>";
      }
    }
    if(isset($_POST['btnAddTaxCategory'])) {
      $insertParams = array();
      if(strlen($_POST['txtName'])>0) $insertParams['name'] = str_replace('"',"`",str_replace("'","`",$_POST['txtName']));
			$taxCategoriesLastId=$taxCategories->insertTaxCategory($insertParams);
			if($taxCategoriesLastId>0){
        echo "<div class='alert alert-success' role='alert'>
          <strong><span class='glyphicon glyphicon-ok'></span> Success!!!</strong> Record has been added successfully.
          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>
          </button>
        </div>";
      }else{
        echo "<div class='alert alert-danger' role='alert'>
          <strong><span class='glyphicon glyphicon-warning-sign'></span> Warning!!!</strong> An error has occurred. Please try again later.
          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>
          </button>
        </div>";
      }
    }
    $taxParams = array("id>" => 0);
		$strTaxations=$taxations->fetchTaxations($taxParams);
    $taxCatsParams = array("id>" => 0);
		$strTaxCategories=$taxCategories->fetchTaxCategories($taxCatsParams);
    echo "<form action='/..$_SERVER[REQUEST_URI]' method='post'>
			<div class='modal fade' id='addTaxation' tabindex='-1' role='dialog' aria-labelledby='formTitle' aria-hidden='true'>
				<div class='modal-dialog'>
					<div class='modal-content'>
						<div class='modal-header'>
							<button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span></button>
							<h4 class='modal-title' id='formTitle'><i class='fa fa-list-ul'></i> New Deadline</h4>
						</div>
						<div class='modal-body'>
              <p>Name <span class='required'>*</span><input type='text' name='txtName' class='form-control' required/></p>
              <p>Date <span class='required'>*</span><input type='date' name='txtDate' class='form-control' required/></p>
              <p>Taxation <span class='required'>*</span>
								<select class='form-control' name='txtCategory' required>
									<option value=''>-- Choose --</option>";
                  for($i=0;$i<sizeof($strTaxCategories);$i++){
                    $TCRow = $strTaxCategories[$i];
										echo "<option value='$TCRow[id]'>$TCRow[name]</option>";
									}
								echo "</select>
							</p>
              <p>Description <span class='required'>*</span><input type='text' name='txtDescription' class='form-control' required/></p>
            </div>
						<div class='modal-footer'>
							<div align='left'><span class='required'>Fields with (*) are required.</span></div>
							<button type='button' class='shadow-z-2 btn btn-default' data-dismiss='modal'><span class='glyphicon glyphicon-log-out'></span> Close</button>
							<button type='submit' name='btnAddTaxation' class='shadow-z-2 btn btn-success'><span class='glyphicon glyphicon-floppy-disk'></span> Add</button>
						</div>
					</div>
				</div>
			</div>
		</form>
    <form action='/..$_SERVER[REQUEST_URI]' method='post'>
			<div class='modal fade' id='addCategory' tabindex='-1' role='dialog' aria-labelledby='formTitle' aria-hidden='true'>
				<div class='modal-dialog'>
					<div class='modal-content'>
						<div class='modal-header'>
							<button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span></button>
							<h4 class='modal-title' id='formTitle'><i class='fa fa-percent'></i> New Taxation</h4>
						</div>
						<div class='modal-body'>
              <p>Name <span class='required'>*</span><input type='text' name='txtName' class='form-control' required/></p>
            </div>
						<div class='modal-footer'>
							<div align='left'><span class='required'>Fields with (*) are required.</span></div>
							<button type='button' class='shadow-z-2 btn btn-default' data-dismiss='modal'><span class='glyphicon glyphicon-log-out'></span> Close</button>
							<button type='submit' name='btnAddTaxCategory' class='shadow-z-2 btn btn-success'><span class='glyphicon glyphicon-floppy-disk'></span> Add</button>
						</div>
					</div>
				</div>
			</div>
		</form>
    <div class='row'>
      <div class='col-md-8 col-xs-12'>
        <div class='x_panel shadow-z-2'>
          <div class='x_title'>
            <h4 style='float: left;'>Deadlines</h4><a href='#' data-toggle='modal' data-target='#addTaxation' data-backdrop='static' class='btn btn-primary' style='margin-left: 20px;'><i class='fa fa-list-ul'></i> Add New Deadline</a>
            <ul class='nav navbar-right panel_toolbox'>
              <li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>
              <li><a class='close-link'><i class='fa fa-close'></i></a></li>
            </ul>
            <div class='clearfix'></div>
          </div>
          <div class='x_content'>";
          if(sizeof($strTaxations)==0){
            echo "<div class='alert alert-danger' role='alert'>No data found.</div>";
          }else{
            echo "<table class='table table-striped table-hover'>
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Date</th>
                  <th>Description</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>";
              for($i=0;$i<sizeof($strTaxations);$i++){
                $TRow = $strTaxations[$i];
                echo "<tr>
                  <td>$TRow[name]</td>
                  <td>$TRow[date]</td>
                  <td>$TRow[description]</td>
                  <td>
                    <a href='/../ViewTaxation/$TRow[uniqId]'><i class='fa fa-eye'></i></a>
                    <a href='/../EditTaxation/$TRow[uniqId]'><i class='fa fa-pencil'></i></a>
                  </td>
                </tr>";
              }
              echo "</tbody>
            </table>";
          }
          echo "</div>
        </div>
      </div>
      <div class='col-md-4 col-xs-12'>
        <div class='x_panel shadow-z-2'>
          <div class='x_title'>
            <h4 style='float: left;'>Taxations</h4><a href='#' data-toggle='modal' data-target='#addCategory' data-backdrop='static' class='btn btn-primary' style='margin-left: 20px;'><i class='fa fa-percent'></i> Add New Taxation</a>
            <ul class='nav navbar-right panel_toolbox'>
              <li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>
              <li><a class='close-link'><i class='fa fa-close'></i></a></li>
            </ul>
            <div class='clearfix'></div>
          </div>
          <div class='x_content'>";
          if(sizeof($strTaxCategories)==0){
            echo "<div class='alert alert-danger' role='alert'>No data found.</div>";
          }else{
            echo "<table class='table table-striped table-hover'>
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>";
              for($i=0;$i<sizeof($strTaxCategories);$i++){
                $TCRow = $strTaxCategories[$i];
                echo "<tr>
                  <td>$TCRow[name]</td>
                  <td>
                    <a href='/../ViewTaxation/$TCRow[uniqId]'><i class='fa fa-eye'></i></a>
                    <a href='/../EditTaxation/$TCRow[uniqId]'><i class='fa fa-pencil'></i></a>
                  </td>
                </tr>";
              }
              echo "</tbody>
            </table>";
          }
          echo "</div>
        </div>
      </div>
    </div>";

    include_once "pages/footer.php";
  }else header("Location:/../Logout");
?>
