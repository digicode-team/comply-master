<?php
  session_name("ComplyMaster");
  session_start();
  if((isset($_SESSION["tout"]))&&($_SESSION["tout"]>time())) {
    $pTime = time();
    $expTime = $pTime + 600;
    $_SESSION["tout"] = $expTime;
    header ("Expires: ".gmdate("D, d M Y H:i:s", time())." GMT");
    header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header ("Cache-Control: no-cache, must-revalidate");
    header ("Pragma: no-cache");
    $title = 'Users';
    include_once "pages/header.php";
    require_once "config.php";
    require_once "DAO/users.php";
    $db = new Database();
    $users = new Users($db);
    echo "<h2>User Details</h2>";
    if(isset($args[2])){
      $parameters = array(
        "uniqId=" => $args[2]
      );
      $strUsers = $users->fetchUsers($parameters);
      if(sizeof($strUsers)==0){
        echo "<div class='alert alert-danger' role='alert'>
          <strong><i class='demo-icon icon-attention'>&#xe80a;</i> Warning!!!</strong> The User does not exists.
        </div>";
      }else{
        if($_SESSION["userAccount"]==1){
          if(isset($_POST['btnSaveChanges'])){
            $updateValues['name'] = str_replace('"',"`",str_replace("'","`",$_POST['txtName']));
            $updateValues['surname'] = str_replace('"',"`",str_replace("'","`",$_POST['txtSurname']));
            $updateValues['email'] = str_replace('"',"`",str_replace("'","`",$_POST['txtEmail']));
            $updateParams = array(
              "uniqId=" => $args[2]
            );
            $statement=$users->updateUser($updateValues,$updateParams);
            if($statement==1){
              echo "<div class='alert alert-success' role='alert'>
                <strong><span class='glyphicon glyphicon-ok'></span> Success!!!</strong> Record has been updated successfully.
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                  <span aria-hidden='true'>&times;</span>
                </button>
              </div>";
            }else{
              echo "<div class='alert alert-danger' role='alert'>
                <strong><span class='glyphicon glyphicon-warning-sign'></span> Warning!!!</strong> An error has occurred. Please try again later.
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                  <span aria-hidden='true'>&times;</span>
                </button>
              </div>";
            }
          }
        }
        $strUsers = $users->fetchUsers($parameters);
        $URow = $strUsers[0];
        if($_SESSION["userAccount"]==1) echo "<form action='/..$_SERVER[REQUEST_URI]' method='post'>";
          echo "<fieldset style='font-weight:bold'>
            <legend>Contact Details</legend>
            <dl class='dl-horizontal'>
              <dt>Name <span class='required'>*</span> :</dt><dd><input type='text' name='txtName' class='form-control' value='$URow[name]' required /></dd>
              <dt>Surname <span class='required'>*</span> :</dt><dd><input type='text' name='txtSurname' class='form-control' value='$URow[surname]' required /></dd>
              <dt>Email <span class='required'>*</span> :</dt><dd><input type='text' name='txtEmail' class='form-control' value='$URow[email]' required /></dd>
            </dl>
          </fieldset>
          <div style='text-align:right'>
            <button type='submit' name='btnSaveChanges' class='btn btn-md btn-success'><span class='glyphicon glyphicon-floppy-disk'></span> Save Changes</button>
          </div>
        </form>";
      }
    }else echo "<div class='alert alert-danger' role='alert'>
      <strong><i class='demo-icon icon-attention'>&#xe80a;</i> Warning!!!</strong> No User selected.
    </div>";
    include_once "pages/footer.php";
  }else header("Location:/../Logout");
?>
