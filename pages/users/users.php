<?php
  session_name("ComplyMaster");
  session_start();
  if((isset($_SESSION["tout"]))&&($_SESSION["tout"]>time())) {
    $pTime = time();
    $expTime = $pTime + 600;
    $_SESSION["tout"] = $expTime;
    header ("Expires: ".gmdate("D, d M Y H:i:s", time())." GMT");
    header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header ("Cache-Control: no-cache, must-revalidate");
    header ("Pragma: no-cache");
    $title = 'Users';
    include_once "pages/header.php";
    require_once "config.php";
    require_once "DAO/users.php";
    $db = new Database();
    $users = new Users($db);
    if(isset($_POST['btnAddUser'])) {
      $insertParams = array();
      if(strlen($_POST['txtUsername'])>0) $insertParams['username'] = str_replace('"',"`",str_replace("'","`",$_POST['txtUsername']));
      if(strlen($_POST['txtPassword'])>0) $insertParams['password'] = $db->encrypt(str_replace('"',"`",str_replace("'","`",$_POST['txtPassword'])));
      if(strlen($_POST['txtName'])>0) $insertParams['name'] = str_replace('"',"`",str_replace("'","`",$_POST['txtName']));
      if(strlen($_POST['txtSurname'])>0) $insertParams['surname'] = str_replace('"',"`",str_replace("'","`",$_POST['txtSurname']));
      if(strlen($_POST['txtEmail'])>0) $insertParams['email'] = str_replace('"',"`",str_replace("'","`",$_POST['txtEmail']));
			$usersLastId=$users->insertUser($insertParams);
			if($usersLastId>0){
        echo "<div class='alert alert-success' role='alert'>
          <strong><span class='glyphicon glyphicon-ok'></span> Success!!!</strong> Record has been added successfully.
          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>
          </button>
        </div>";
      }else{
        echo "<div class='alert alert-danger' role='alert'>
          <strong><span class='glyphicon glyphicon-warning-sign'></span> Warning!!!</strong> An error has occurred. Please try again later.
          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>
          </button>
        </div>";
      }
    }
    $parameters = array("account=" => 1);
		$strUsers=$users->fetchUsers($parameters);
    echo "<form action='/..$_SERVER[REQUEST_URI]' method='post'>
			<div class='modal fade' id='addUser' tabindex='-1' role='dialog' aria-labelledby='formTitle' aria-hidden='true'>
				<div class='modal-dialog'>
					<div class='modal-content'>
						<div class='modal-header'>
							<button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span></button>
							<h4 class='modal-title' id='formTitle'><i class='fa fa-user-plus'></i> New User</h4>
						</div>
						<div class='modal-body'>
              <div class='row'>
                <div class='col-md-6'>Username <span class='required'>*</span><input type='text' name='txtUsername' class='form-control' required/></div>
                <div class='col-md-6'>Password <span class='required'>*</span><input type='password' name='txtPassword' class='form-control' required/></div>
              </div>
              <div class='row'>
                <div class='col-md-6'>Name <span class='required'>*</span><input type='text' name='txtName' class='form-control' required/></div>
                <div class='col-md-6'>Surname <span class='required'>*</span><input type='text' name='txtSurname' class='form-control' required/></div>
              </div>
              <div class='row'>
                <div class='col-sm-12'>Email <span class='required'>*</span><input type='email' name='txtEmail' class='form-control' required/></div>
              </div>
            </div>
						<div class='modal-footer'>
							<div align='left'><span class='required'>Fields with (*) are required.</span></div>
							<button type='button' class='shadow-z-2 btn btn-default' data-dismiss='modal'><span class='glyphicon glyphicon-log-out'></span> Close</button>
							<button type='submit' name='btnAddUser' class='shadow-z-2 btn btn-success'><span class='glyphicon glyphicon-floppy-disk'></span> Add</button>
						</div>
					</div>
				</div>
			</div>
		</form>
    <div class='row'>
      <div class='col-xs-12'>
        <div class='x_panel shadow-z-2'>
          <div class='x_title'>
            <h2>Users</h2><a href='#' data-toggle='modal' data-target='#addUser' data-backdrop='static' class='btn btn-primary' style='margin-left: 20px;'><i class='fa fa-user-plus'></i> Add New User</a>
            <ul class='nav navbar-right panel_toolbox'>
              <li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>
              <li><a class='close-link'><i class='fa fa-close'></i></a></li>
            </ul>
            <div class='clearfix'></div>
          </div>
          <div class='x_content'>";
          if(sizeof($strUsers)==0){
            echo "<div class='alert alert-danger' role='alert'>No data found.</div>";
          }else{
            echo "<table class='table table-striped table-hover'>
              <thead>
                <tr>
                  <th>Username</th>
                  <th>Login</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>";
              for($i=0;$i<sizeof($strUsers);$i++){
                $URow = $strUsers[$i];
                echo "<tr>
                  <td>$URow[username]</td>
                  <td>
                    <input type='checkbox' id='cmdLogin' name='cmdLogin' onclick='changeLogin(\"$URow[uniqId]\")'";
                      if($URow['login']==1) echo " checked";
                      echo ">
                  </td>
                  <td>
                    <a href='/../ViewUser/$URow[uniqId]'><i class='fa fa-eye'></i></a>
                    <a href='/../EditUser/$URow[uniqId]'><i class='fa fa-pencil'></i></a>
                  </td>
                </tr>";
              }
              echo "</tbody>
            </table>";
          }
          echo "</div>
        </div>
      </div>
    </div>
    <script type='text/javascript'>
			function changeLogin(user){
				$.ajax({
					url:'/../ChangeLoginUser',
					type:'POST',
					data: 'user='+user,
					success: function (data) {
            if(data == 1){
							alert('Login status changed successfully');
						}else{
							alert(data);
						}
					},
					error:function(){
						alert('An error has occurred. Please try again later.');
					}
				})
			}
		</script>";
    include_once "pages/footer.php";
  }else header("Location:/../Logout");
?>
