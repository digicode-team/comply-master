<?php
  session_name("ComplyMaster");
  session_start();
  if((isset($_SESSION["tout"]))&&($_SESSION["tout"]>time())) {
    $pTime = time();
    $expTime = $pTime + 600;
    $_SESSION["tout"] = $expTime;
    header ("Expires: ".gmdate("D, d M Y H:i:s", time())." GMT");
    header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header ("Cache-Control: no-cache, must-revalidate");
    header ("Pragma: no-cache");
    $title = 'View User';
    include_once "pages/header.php";
    require_once "config.php";
    require_once "DAO/users.php";
    $db = new Database();
    $users = new Users($db);
    echo "<h2>User Details</h2>";
    if(isset($args[2])){
      $parameters = array(
        "uniqId=" => $args[2]
      );
      $strUsers = $users->fetchUsers($parameters);
      if(sizeof($strUsers)==0){
        echo "<div class='alert alert-danger' role='alert'>
          <strong><span class='glyphicon glyphicon-warning-sign'></span> Warning!!!</strong> The User does not exists.
        </div>";
      }else{
        $URow = $strUsers[0];
        echo "<fieldset style='font-weight:bold'>
          <legend>General Details</legend>
          <dl class='dl-horizontal'>
            <dt>Username :</dt><dd>$URow[username]</dd>
            <dt>Name :</dt><dd>$URow[name]</dd>
            <dt>Surname :</dt><dd>$URow[surname]</dd>
            <dt>Email :</dt><dd>$URow[email]</dd>
          </dl>
        </fieldset>";
      }
    }else echo "<div class='alert alert-danger' role='alert'>
      <strong><span class='glyphicon glyphicon-warning-sign'></span> Warning!!!</strong> No User selected.
    </div>";
    include_once "pages/footer.php";
  }else header("Location:/../Logout");
?>
