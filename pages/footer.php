            </div>
          </div>
        </div>
      </div>
      <footer style='padding-left: 250px;'>
        <div class='pull-left'>
          <strong>Copyright © 2019 Comply Master.</strong> All rights reserved.
        </div>
        <div class="pull-right">
          Powered by <a href="https://www.innoviumdigital.com/">Innovium Digital</a>
        </div>
        <div class="clearfix"></div>
      </footer>
    </div>
  </div>
  <script src="/../js/jquery.js" type="text/javascript"></script>
  <script src="/../js/bootstrap.js" type="text/javascript"></script>
  <script src="/../js/fastclick.js" type="text/javascript"></script>
  <script src="/../js/nprogress.js" type="text/javascript"></script>
  <script src="/../js/custom.js" type="text/javascript"></script>
</html>
