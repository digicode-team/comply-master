<?php
  echo "<div class='col-md-3 left_col'>
    <div class='left_col scroll-view'>
      <div class='navbar nav_title' style='border: 0;'>
        <a href='index.html' class='site_title'><img src='/../images/logo.png' style='max-width: 15%;'> <span>Comply Master</span></a>
      </div>
      <div class='clearfix'></div>
      <br />
      <div id='sidebar-menu' class='main_menu_side hidden-print main_menu'>
        <div class='menu_section'>
          <h3>General</h3>
          <ul class='nav side-menu'>
            <li><a href='/../Dashboard'><i class='fa fa-dashboard'></i> Dashboard</a></li>";
            if($_SESSION['userAccount']==1)
              echo "<li><a href='/../Users'><i class='fa fa-user'></i> Users</a></li>
              <li><a href='/../Agents'><i class='fa fa-users'></i> Agents</a></li>";
            else
              echo "<li><a href='/../Clients'><i class='fa fa-users'></i> Clients</a></li>";
          echo "</ul>
        </div>";
        if($_SESSION['userAccount']==1)
          echo "<div class='menu_section'>
            <h3>Administration</h3>
            <ul class='nav side-menu'>
              <li><a href='/../Taxations'><i class='fa fa-percent'></i> Taxations</a></li>
            </ul>
          </div>";
        echo "<div class='menu_section'>
          <h3>Reports</h3>
          <ul class='nav side-menu'>
            <li><a href='/../Report'><i class='fa fa-print'></i> Date Range</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class='top_nav'>
    <div class='nav_menu'>
      <nav>
        <div class='nav toggle'>
          <a id='menu_toggle'><i class='fa fa-bars'></i></a>
        </div>
        <ul class='nav navbar-nav navbar-right'>
          <li class=''>
            <a href='javascript:;' class='user-profile dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>
              <img src='images/img.jpg' alt=''>$_SESSION[userName]
              <span class=' fa fa-angle-down'></span>
            </a>
            <ul class='dropdown-menu dropdown-usermenu pull-right'>
              <li><a href='/../Profile'><i class='fa fa-user'></i>  Profile</a></li>
              <li><a href='/../Settings'><i class='fa fa-gear'></i> Settings</a></li>
              <li><a href='/../Logout'><i class='fa fa-sign-out'></i> Log Out</a></li>
            </ul>
          </li>
          <li role='presentation' class='dropdown'>
            <a href='javascript:;' class='dropdown-toggle info-number' data-toggle='dropdown' aria-expanded='false'>
              <i class='fa fa-envelope-o'></i>
              <span class='badge bg-green'>6</span>
            </a>
            <ul id='menu1' class='dropdown-menu list-unstyled msg_list' role='menu'>
              <li>
                <a>
                  <span class='image'><img src='images/img.jpg' alt='Profile Image' /></span>
                  <span>
                    <span>John Smith</span>
                    <span class='time'>3 mins ago</span>
                  </span>
                  <span class='message'>
                    Film festivals used to be do-or-die moments for movie makers. They were where...
                  </span>
                </a>
              </li>
              <li>
                <a>
                  <span class='image'><img src='images/img.jpg' alt='Profile Image' /></span>
                  <span>
                    <span>John Smith</span>
                    <span class='time'>3 mins ago</span>
                  </span>
                  <span class='message'>
                    Film festivals used to be do-or-die moments for movie makers. They were where...
                  </span>
                </a>
              </li>
              <li>
                <a>
                  <span class='image'><img src='images/img.jpg' alt='Profile Image' /></span>
                  <span>
                    <span>John Smith</span>
                    <span class='time'>3 mins ago</span>
                  </span>
                  <span class='message'>
                    Film festivals used to be do-or-die moments for movie makers. They were where...
                  </span>
                </a>
              </li>
              <li>
                <a>
                  <span class='image'><img src='images/img.jpg' alt='Profile Image' /></span>
                  <span>
                    <span>John Smith</span>
                    <span class='time'>3 mins ago</span>
                  </span>
                  <span class='message'>
                    Film festivals used to be do-or-die moments for movie makers. They were where...
                  </span>
                </a>
              </li>
              <li>
                <div class='text-center'>
                  <a>
                    <strong>See All Alerts</strong>
                    <i class='fa fa-angle-right'></i>
                  </a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
    </div>
  </div>";
?>
