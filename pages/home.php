<?php
session_name("ComplyMaster");
session_start();
header ("Expires: ".gmdate("D, d M Y H:i:s", time())." GMT");
header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header ("Cache-Control: no-cache, must-revalidate");
header ("Pragma: no-cache");
if((isset($_SESSION["tout"]))&&($_SESSION["tout"]>time())) {
	header("Location:/../Dashboard");
}else{
	$canLogin = true;
	$errorLogin = "";
	$pTime = time();
	$expTime = $pTime + 600;
	if(isset($_SESSION["tout"])){unset($_SESSION["tout"]);}
	if(isset($_SESSION["user_id"])){unset($_SESSION["user_id"]);}
	if(isset($_POST['btnLogin'])) {
		require_once "config.php";
		require_once "DAO/users.php";
		$db = new Database();
		$users = new Users($db);
		$username=$_POST['txtUsername'];
		$password=$_POST['txtPassword'];
		$password = $db->encrypt($password);
		$parameters = array(
			"username=" => $username
		);
		$checkUsername=$users->fetchUserLogin($parameters);
		if(sizeof($checkUsername)!=0){
			if($checkUsername['password']=="$password"){
				if($checkUsername['account']<=2){
					if($checkUsername['login']){
						$_SESSION["tout"] = $expTime;
						$_SESSION["userId"] = $checkUsername['id'];
						$_SESSION["userUniqId"] = $checkUsername['uniqId'];
						$_SESSION["userName"] = $checkUsername['username'];
						$_SESSION["userFirstname"] = $checkUsername['name'];
            $_SESSION["userLastname"] = $checkUsername['surname'];
						$_SESSION["userAccount"] = $checkUsername['account'];
						header("Location:/../Dashboard");
					}else{
						$canLogin = false;
						$errorLogin = 'Your account is not activated. Please contact with administrator.';
					}
				}else{
					$canLogin = false;
					$errorLogin = 'You are not allowed to login this panel.';
				}
			}else{
				$canLogin = false;
				$errorLogin = 'Wrong Password.';
			}
		}else{
		  $canLogin = false;
		  $errorLogin = 'Wrong Username.';
		}
	}
	echo "<!DOCTYPE html>
	<html lang='en' xmlns:og='http://ogp.me/ns#' xmlns:fb='https://www.facebook.com/2008/fbml'>
		<head>
			<title>Comply Master</title>
			<meta charset='utf-8'>
			<meta http-equiv='X-UA-Compatible' content='IE=edge'>
			<meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'>
			<META NAME='Keywords' CONTENT=''>
			<META NAME='Description' CONTENT=''>
			<META http-equiv='content-language' content='en'>
			<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
			<META NAME='author' CONTENT='Comply Master'>
			<link type='text/css' rel='stylesheet' href='/../css/bootstrap.css'/>
      <link type='text/css' rel='stylesheet' href='/../css/font-awesome.min.css'>
			<link rel='shortcut icon' href='/../favicon.ico' type='image/x-icon'>
			<link rel='icon' href='/../favicon.ico' type='image/x-icon'>
			<script type='text/javascript' src='/../js/jquery.js'></script>
			<script type='text/javascript' src='/../js/bootstrap.js'></script>
			<style>
		    body {
		        background: url('images/background.png') no-repeat center center fixed;
		        -webkit-background-size: cover;
		        -moz-background-size: cover;
		        -o-background-size: cover;
		        background-size: cover;
		    }
		</style>
		</head>
		<body>
			<nav class='navbar navbar-default navbar-fixed-top'>
				<div class='container'>
					<div class='navbar-header'>
						<button type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#navbar' aria-expanded='false' aria-controls='navbar'>
							<span class='sr-only'>Toggle navigation</span>
							<span class='icon-bar'></span>
							<span class='icon-bar'></span>
							<span class='icon-bar'></span>
						</button>
						<a class='navbar-brand' href='/'><img src='/../images/logo-full.png' style='height:100%;'></a>
					</div>
					<div id='navbar' class='navbar-collapse collapse'>
						<ul class='nav navbar-nav'>
							<li class='active'><a href='#'>Home</a></li>
							<li><a href='#'>About</a></li>
							<li><a href='#'>Contact</a></li>
						</ul>
						<ul class='nav navbar-nav navbar-right'>
							<li><a href='#' data-toggle='modal' data-target='#Login' data-backdrop='static'>Login</a></li>
						</ul>
					</div><!--/.nav-collapse -->
				</div>
			</nav>
      <main role='main' class='container'>
				<form action='/..$_SERVER[REQUEST_URI]' method='post' style='margin-block-end:0px'>
					<div class='modal fade' id='Login' tabindex='-1' role='dialog' aria-labelledby='formTitle' aria-hidden='true'>
						<div class='modal-dialog modal-sm'>
							<div class='modal-content'>
								<div class='modal-header'>
									<button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span></button>
									<h4 class='modal-title' id='formTitle'><i class='fa fa-sign-in'></i> Login</h4>
								</div>
								<div class='modal-body'>
                  <p>Username<input type='text' name='txtUsername' class='form-control empty' autofocus required></p>
    							<p>Password<input type='password' name='txtPassword' class='form-control empty' required></p>
                </div>
  							<div class='modal-footer'>
  								<button class='btn btn-success btn-block' type='submit' id='btnLogin' name='btnLogin'><i class='fa fa-sign-in'></i> Login</button>
  							</div>
	  					</div>
	  				</div>
	  			</div>
				</form>
      </main>
		</body>
	</html>";
}
?>
