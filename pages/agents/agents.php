<?php
  session_name("ComplyMaster");
  session_start();
  if((isset($_SESSION["tout"]))&&($_SESSION["tout"]>time())) {
    $pTime = time();
    $expTime = $pTime + 600;
    $_SESSION["tout"] = $expTime;
    header ("Expires: ".gmdate("D, d M Y H:i:s", time())." GMT");
    header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header ("Cache-Control: no-cache, must-revalidate");
    header ("Pragma: no-cache");
    $title = 'Agents';
    include_once "pages/header.php";
    require_once "config.php";
    require_once "DAO/users.php";
    $db = new Database();
    $users = new Users($db);
    if(isset($_POST['btnAddAgent'])) {
      $insertParams = array();
      if(strlen($_POST['txtUsername'])>0) $insertParams['username'] = str_replace('"',"`",str_replace("'","`",$_POST['txtUsername']));
      if(strlen($_POST['txtPassword'])>0) $insertParams['password'] = $db->encrypt(str_replace('"',"`",str_replace("'","`",$_POST['txtPassword'])));
      if(strlen($_POST['txtName'])>0) $insertParams['name'] = str_replace('"',"`",str_replace("'","`",$_POST['txtName']));
      if(strlen($_POST['txtSurname'])>0) $insertParams['surname'] = str_replace('"',"`",str_replace("'","`",$_POST['txtSurname']));
      if(strlen($_POST['txtCompanyName'])>0) $insertParams['company'] = str_replace('"',"`",str_replace("'","`",$_POST['txtCompanyName']));
      if(strlen($_POST['txtAddress'])>0) $insertParams['address'] = str_replace('"',"`",str_replace("'","`",$_POST['txtAddress']));
      if(strlen($_POST['txtPostCode'])>0) $insertParams['postCode'] = str_replace('"',"`",str_replace("'","`",$_POST['txtPostCode']));
      if(strlen($_POST['txtCity'])>0) $insertParams['city'] = str_replace('"',"`",str_replace("'","`",$_POST['txtCity']));
      if(strlen($_POST['txtCountry'])>0) $insertParams['country'] = str_replace('"',"`",str_replace("'","`",$_POST['txtCountry']));
      if(strlen($_POST['txtTIC'])>0) $insertParams['tic'] = str_replace('"',"`",str_replace("'","`",$_POST['txtTIC']));
      if(strlen($_POST['txtTel'])>0) $insertParams['tel'] = str_replace('"',"`",str_replace("'","`",$_POST['txtTel']));
      if(strlen($_POST['txtMob'])>0) $insertParams['mob'] = str_replace('"',"`",str_replace("'","`",$_POST['txtMob']));
      if(strlen($_POST['txtEmail'])>0) $insertParams['email'] = str_replace('"',"`",str_replace("'","`",$_POST['txtEmail']));
      if(isset($_POST['chkBilling'])){
        if(strlen($_POST['txtCompanyName'])>0) $insertParams['billingCompany'] = str_replace('"',"`",str_replace("'","`",$_POST['txtCompanyName']));
        if(strlen($_POST['txtAddress'])>0) $insertParams['billingAddress'] = str_replace('"',"`",str_replace("'","`",$_POST['txtAddress']));
        if(strlen($_POST['txtPostCode'])>0) $insertParams['billingPostCode'] = str_replace('"',"`",str_replace("'","`",$_POST['txtPostCode']));
        if(strlen($_POST['txtCity'])>0) $insertParams['billingCity'] = str_replace('"',"`",str_replace("'","`",$_POST['txtCity']));
        if(strlen($_POST['txtCountry'])>0) $insertParams['billingCountry'] = str_replace('"',"`",str_replace("'","`",$_POST['txtCountry']));
        if(strlen($_POST['txtTIC'])>0) $insertParams['billingVAT'] = str_replace('"',"`",str_replace("'","`",$_POST['txtTIC']));
      }else{
        if(strlen($_POST['txtBillingCompanyName'])>0) $insertParams['billingCompany'] = str_replace('"',"`",str_replace("'","`",$_POST['txtBillingCompanyName']));
        if(strlen($_POST['txtBillingAddress'])>0) $insertParams['billingAddress'] = str_replace('"',"`",str_replace("'","`",$_POST['txtBillingAddress']));
        if(strlen($_POST['txtBillingPostCode'])>0) $insertParams['billingPostCode'] = str_replace('"',"`",str_replace("'","`",$_POST['txtBillingPostCode']));
        if(strlen($_POST['txtBillingCity'])>0) $insertParams['billingCity'] = str_replace('"',"`",str_replace("'","`",$_POST['txtCity']));
        if(strlen($_POST['txtBillingCountry'])>0) $insertParams['billingCountry'] = str_replace('"',"`",str_replace("'","`",$_POST['txtBillingCountry']));
        if(strlen($_POST['txtBillingVAT'])>0) $insertParams['billingVAT'] = str_replace('"',"`",str_replace("'","`",$_POST['txtBillingVAT']));
      }
      if(strlen($_POST['txtEmailUsername'])>0) $insertParams['emailUsername'] = str_replace('"',"`",str_replace("'","`",$_POST['txtEmailUsername']));
      if(strlen($_POST['txtEmailPassword'])>0) $insertParams['emailPassword'] = str_replace('"',"`",str_replace("'","`",$_POST['txtEmailPassword']));
      if(strlen($_POST['txtOutgoingServer'])>0) $insertParams['emailServer'] = str_replace('"',"`",str_replace("'","`",$_POST['txtOutgoingServer']));
      if(strlen($_POST['txtPort'])>0) $insertParams['emailPort'] = str_replace('"',"`",str_replace("'","`",$_POST['txtPort']));
			$agentsLastId=$users->insertAgent($insertParams);
			if($agentsLastId>0){
        echo "<div class='alert alert-success' role='alert'>
          <strong><span class='glyphicon glyphicon-ok'></span> Success!!!</strong> Record has been added successfully.
          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>
          </button>
        </div>";
      }else{
        echo "<div class='alert alert-danger' role='alert'>
          <strong><span class='glyphicon glyphicon-warning-sign'></span> Warning!!!</strong> An error has occurred. Please try again later.
          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>
          </button>
        </div>";
      }
    }
    $agentsParameters = array("account=" => 2);
		$strUsers=$users->fetchUsers($agentsParameters);
    echo "<form action='/..$_SERVER[REQUEST_URI]' method='post'>
			<div class='modal fade' id='addCustomer' tabindex='-1' role='dialog' aria-labelledby='formTitle' aria-hidden='true'>
				<div class='modal-dialog'>
					<div class='modal-content'>
						<div class='modal-header'>
							<button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span></button>
							<h4 class='modal-title' id='formTitle'><i class='fa fa-user-plus'></i> New Agent</h4>
						</div>
						<div class='modal-body'>
              <fieldset style='font-weight:bold'>
                <legend>General Details</legend>
                <div class='row'>
                  <div class='col-md-6'>Username <span class='required'>*</span><input type='text' name='txtUsername' class='form-control' required/></div>
                  <div class='col-md-6'>Password <span class='required'>*</span><input type='password' name='txtPassword' class='form-control' required/></div>
                </div>
                <div class='row'>
                  <div class='col-sm-12'>Company Name<input type='text' name='txtCompanyName' class='form-control'/></div>
                </div>
                <div class='row'>
                  <div class='col-sm-12'>Address<input type='text' name='txtAddress' class='form-control'/></div>
                </div>
                <div class='row'>
                  <div class='col-md-4'>Post Code<input type='number' name='txtPostCode' class='form-control'/></div>
                  <div class='col-md-4'>City<input type='text' name='txtCity' class='form-control'/></div>
                  <div class='col-md-4'>Country<input type='text' name='txtCountry' class='form-control'/></div>
                </div>
                <div class='row'>
                  <div class='col-sm-12'>TIC<input type='text' name='txtTIC' class='form-control'/></div>
                </div>
              </fieldset>
              <fieldset style='font-weight:bold'>
                <legend>Contact Details</legend>
                <div class='row'>
                  <div class='col-md-6'>Name <span class='required'>*</span><input type='text' name='txtName' class='form-control' required/></div>
                  <div class='col-md-6'>Surname <span class='required'>*</span><input type='text' name='txtSurname' class='form-control' required/></div>
                </div>
                <div class='row'>
                  <div class='col-md-6'>Telephone<input type='text' name='txtTel' class='form-control'/></div>
                  <div class='col-md-6'>Cellphone<input type='text' name='txtMob' class='form-control'/></div>
                </div>
                <div class='row'>
                  <div class='col-sm-12'>Email <span class='required'>*</span><input type='email' name='txtEmail' class='form-control' required/></div>
                </div>
              </fieldset>
              <fieldset style='font-weight:bold'>
                <legend>Billing Details</legend>
                <div calss='row'> <input type='checkbox' id='chkBilling' name='chkBilling' onclick='checkBilling()' value='1' checked> Same as General Details</div>
                <div class='row' id='divBillingCompanyName' hidden>
                  <div class='col-sm-12'>Company Name<input type='text' name='txtBillingCompanyName' class='form-control'/></div>
                </div>
                <div class='row' id='divBillingAddress' hidden>
                  <div class='col-sm-12'>Address<input type='text' name='txtBillingAddress' class='form-control'/></div>
                </div>
                <div class='row' id='divBillingAddress2' hidden>
                  <div class='col-md-4'>Post Code<input type='number' name='txtBillingPostCode' class='form-control'/></div>
                  <div class='col-md-4'>City<input type='text' name='txtBillingCity' class='form-control'/></div>
                  <div class='col-md-4'>Country<input type='text' name='txtBillingCountry' class='form-control'/></div>
                </div>
                <div class='row' id='divBillingVAT' hidden>
                  <div class='col-sm-12'>VAT<input type='text' name='txtBillingVAT' class='form-control'/></div>
                </div>
              </fieldset>
              <fieldset style='font-weight:bold'>
                <legend>Mail Server Details</legend>
                <div class='row'>
                  <div class='col-sm-6'>Username<input type='text' name='txtEmailUsername' class='form-control'/></div>
                  <div class='col-sm-6'>Password<input type='text' name='txtEmailPassword' class='form-control'/></div>
                </div>
                <div class='row'>
                  <div class='col-sm-8'>Outgoing Server<input type='text' name='txtOutgoingServer' class='form-control'/></div>
                  <div class='col-sm-4'>Port<input type='number' name='txtPort' class='form-control'/></div>
                </div>
              </fieldset>
            </div>
						<div class='modal-footer'>
							<div align='left'><span class='required'>Fields with (*) are required.</span></div>
							<button type='button' class='shadow-z-2 btn btn-default' data-dismiss='modal'><span class='glyphicon glyphicon-log-out'></span> Close</button>
							<button type='submit' name='btnAddAgent' class='shadow-z-2 btn btn-success'><span class='glyphicon glyphicon-floppy-disk'></span> Add</button>
						</div>
					</div>
				</div>
			</div>
		</form>
    <div class='row'>
      <div class='col-xs-12'>
        <div class='x_panel shadow-z-2'>
          <div class='x_title'>
            <h2>Agents</h2><a href='#' data-toggle='modal' data-target='#addCustomer' data-backdrop='static' class='btn btn-primary' style='margin-left: 20px;'><i class='fa fa-user-plus'></i> Add New Agent</a>
            <ul class='nav navbar-right panel_toolbox'>
              <li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>
              <li><a class='close-link'><i class='fa fa-close'></i></a></li>
            </ul>
            <div class='clearfix'></div>
          </div>
          <div class='x_content'>";
          if(sizeof($strUsers)==0){
            echo "<div class='alert alert-danger' role='alert'>No data found.</div>";
          }else{
            echo "<table class='table table-striped table-hover'>
              <thead>
                <tr>
                  <th>Company</th>
                  <th>Contact Name</th>
                  <th>Username</th>
                  <th>Clients</th>
                  <th>Login</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>";
              for($i=0;$i<sizeof($strUsers);$i++){
                $URow = $strUsers[$i];
                echo "<tr>
                  <td>$URow[company]</td>
                  <td>$URow[name] $URow[surname]</td>
                  <td>$URow[username]</td>
                  <td>0</td>
                  <td>
                    <input type='checkbox' id='cmdLogin' name='cmdLogin' onclick='changeLogin(\"$URow[uniqId]\")'";
                      if($URow['login']==1) echo " checked";
                      echo ">
                  </td>
                  <td>
                    <a href='/../ViewAgent/$URow[uniqId]'><i class='fa fa-eye'></i></a>
                    <a href='/../EditAgent/$URow[uniqId]'><i class='fa fa-pencil'></i></a>
                  </td>
                </tr>";
              }
              echo "</tbody>
            </table>";
          }
          echo "</div>
        </div>
      </div>
    </div>
    <script>
      function checkBilling() {
				if(document.getElementById('chkBilling').checked){
          $(document).ready(function(){ $('#divBillingCompanyName').hide(1000); });
          $(document).ready(function(){ $('#divBillingAddress').hide(1000); });
          $(document).ready(function(){ $('#divBillingAddress2').hide(1000); });
          $(document).ready(function(){ $('#divBillingVAT').hide(1000); });
				}else{
          $(document).ready(function(){ $('#divBillingCompanyName').show(1000); });
          $(document).ready(function(){ $('#divBillingAddress').show(1000); });
          $(document).ready(function(){ $('#divBillingAddress2').show(1000); });
          $(document).ready(function(){ $('#divBillingVAT').show(1000); });
				}
			}
			function changeLogin(user){
				$.ajax({
					url:'/../ChangeLoginAgent',
					type:'POST',
					data: 'user='+user,
					success: function (data) {
            if(data == 1){
							alert('Login status changed successfully');
						}else{
							alert(data);
						}
					},
					error:function(){
						alert('An error has occurred. Please try again later.');
					}
				})
			}
		</script>";
    include_once "pages/footer.php";
  }else header("Location:/../Logout");
?>
