<?php
  session_name("ComplyMaster");
  session_start();
  if((isset($_SESSION["tout"]))&&($_SESSION["tout"]>time())) {
    $pTime = time();
    $expTime = $pTime + 600;
    $_SESSION["tout"] = $expTime;
    header ("Expires: ".gmdate("D, d M Y H:i:s", time())." GMT");
    header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header ("Cache-Control: no-cache, must-revalidate");
    header ("Pragma: no-cache");
    $title = 'Edit Agent';
    include_once "pages/header.php";
    require_once "config.php";
    require_once "DAO/users.php";
    $db = new Database();
    $users = new Users($db);
    echo "<h2>Agent Details</h2>";
    if(isset($args[2])){
      $parameters = array(
        "uniqId=" => $args[2]
      );
      $strUsers = $users->fetchUsers($parameters);
      if(sizeof($strUsers)==0){
        echo "<div class='alert alert-danger' role='alert'>
          <strong><span class='glyphicon glyphicon-warning-sign'></span> Warning!!!</strong> The User does not exists.
        </div>";
      }else{
        if($_SESSION["userAccount"]==1){
          if(isset($_POST['btnSaveChanges'])){
            $updateValues['name'] = str_replace('"',"`",str_replace("'","`",$_POST['txtName']));
            $updateValues['surname'] = str_replace('"',"`",str_replace("'","`",$_POST['txtSurname']));
            $updateValues['company'] = str_replace('"',"`",str_replace("'","`",$_POST['txtCompanyName']));
            $updateValues['address'] = str_replace('"',"`",str_replace("'","`",$_POST['txtAddress']));
            $updateValues['postCode'] = str_replace('"',"`",str_replace("'","`",$_POST['txtPostCode']));
            $updateValues['city'] = str_replace('"',"`",str_replace("'","`",$_POST['txtCity']));
            $updateValues['country'] = str_replace('"',"`",str_replace("'","`",$_POST['txtCountry']));
            $updateValues['tic'] = str_replace('"',"`",str_replace("'","`",$_POST['txtTIC']));
            $updateValues['tel'] = str_replace('"',"`",str_replace("'","`",$_POST['txtTel']));
            $updateValues['mob'] = str_replace('"',"`",str_replace("'","`",$_POST['txtMob']));
            $updateValues['email'] = str_replace('"',"`",str_replace("'","`",$_POST['txtEmail']));
            $updateValues['billingCompany'] = str_replace('"',"`",str_replace("'","`",$_POST['txtBillingCompanyName']));
            $updateValues['billingAddress'] = str_replace('"',"`",str_replace("'","`",$_POST['txtBillingAddress']));
            $updateValues['billingPostCode'] = str_replace('"',"`",str_replace("'","`",$_POST['txtBillingPostCode']));
            $updateValues['billingCity'] = str_replace('"',"`",str_replace("'","`",$_POST['txtCity']));
            $updateValues['billingCountry'] = str_replace('"',"`",str_replace("'","`",$_POST['txtBillingCountry']));
            $updateValues['billingVAT'] = str_replace('"',"`",str_replace("'","`",$_POST['txtBillingVAT']));
            $updateValues['emailUsername'] = str_replace('"',"`",str_replace("'","`",$_POST['txtEmailUsername']));
            $updateValues['emailPassword'] = str_replace('"',"`",str_replace("'","`",$_POST['txtEmailPassword']));
            $updateValues['emailServer'] = str_replace('"',"`",str_replace("'","`",$_POST['txtOutgoingServer']));
            $updateValues['emailPort'] = str_replace('"',"`",str_replace("'","`",$_POST['txtPort']));
            $updateParams = array(
              "uniqId=" => $args[2]
            );
            $statement=$users->updateUser($updateValues,$updateParams);
            if($statement==1){
              echo "<div class='alert alert-success' role='alert'>
                <strong><span class='glyphicon glyphicon-ok'></span> Success!!!</strong> Record has been updated successfully.
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                  <span aria-hidden='true'>&times;</span>
                </button>
              </div>";
            }else{
              echo "<div class='alert alert-danger' role='alert'>
                <strong><span class='glyphicon glyphicon-warning-sign'></span> Warning!!!</strong> An error has occurred. Please try again later.
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                  <span aria-hidden='true'>&times;</span>
                </button>
              </div>";
            }
          }
        }
        $strUsers = $users->fetchUsers($parameters);
        $URow = $strUsers[0];
        echo "<form action='/..$_SERVER[REQUEST_URI]' method='post'>
          <fieldset style='font-weight:bold'>
            <legend>Contact Details</legend>
            <legend>General Details</legend>
            <dl class='dl-horizontal'>
              <dt>Username :</dt><dd>$URow[username]</dd>
              <dt>Company Name :</dt><dd><input type='text' name='txtCompanyName' class='form-control' value='$URow[company]' /></dd>
              <dt>Address :</dt><dd><input type='text' name='txtAddress' class='form-control' value='$URow[address]' /></dd>
              <dt>Post Code :</dt><dd><input type='number' name='txtPostCode' class='form-control' value='$URow[postCode]' /></dd>
              <dt>City :</dt><dd><input type='text' name='txtCity' class='form-control' value='$URow[city]' /></dd>
              <dt>Country :</dt><dd><input type='text' name='txtCountry' class='form-control' value='$URow[country]' /></dd>
              <dt>TIC :</dt><dd><input type='text' name='txtTIC' class='form-control' value='$URow[tic]' /></dd>
            </dl>
          </fieldset>
          <fieldset style='font-weight:bold'>
            <legend>Contact Details</legend>
            <dl class='dl-horizontal'>
              <dt>Name <span class='required'>*</span> :</dt><dd><input type='text' name='txtName' class='form-control' value='$URow[name]' required /></dd>
              <dt>Surname <span class='required'>*</span> :</dt><dd><input type='text' name='txtSurname' class='form-control' value='$URow[surname]' required /></dd>
              <dt>Telephone :</dt><dd><input type='text' name='txtTel' class='form-control' value='$URow[telephone]' /></dd>
              <dt>Cellphone :</dt><dd><input type='text' name='txtMob' class='form-control' value='$URow[cellphone]' /></dd>
              <dt>Email <span class='required'>*</span> :</dt><dd><input type='text' name='txtEmail' class='form-control' value='$URow[email]' required /></dd>
            </dl>
          </fieldset>
          <fieldset style='font-weight:bold'>
            <legend>Billing Details</legend>
            <dl class='dl-horizontal'>
              <dt>Company Name :</dt><dd><input type='text' name='txtBillingCompanyName' class='form-control' value='$URow[billingCompany]' /></dd>
              <dt>Address :</dt><dd><input type='text' name='txtBillingAddress' class='form-control' value='$URow[billingAddress]' /></dd>
              <dt>Post Code :</dt><dd><input type='number' name='txtBillingPostCode' class='form-control' value='$URow[billingPostCode]' /></dd>
              <dt>City :</dt><dd><input type='text' name='txtBillingCity' class='form-control' value='$URow[billingCity]' /></dd>
              <dt>Country :</dt><dd><input type='text' name='txtBillingCountry' class='form-control' value='$URow[billingCountry]' /></dd>
              <dt>VAT :</dt><dd><input type='text' name='txtBillingVAT' class='form-control' value='$URow[billingVAT]' /></dd>
            </dl>
          </fieldset>
          <fieldset style='font-weight:bold'>
            <legend>Mail Server Details</legend>
            <dl class='dl-horizontal'>
              <dt>Username :</dt><dd><input type='text' name='txtEmailUsername' class='form-control' value='$URow[emailUsername]' /></dd>
              <dt>Password :</dt><dd><input type='text' name='txtEmailPassword' class='form-control' value='$URow[emailPassword]' /></dd>
              <dt>Outgoing Server :</dt><dd><input type='text' name='txtOutgoingServer' class='form-control' value='$URow[emailServer]' /></dd>
              <dt>Port :</dt><dd><input type='number' name='txtPort' class='form-control' value='$URow[emailPort]' /></dd>
            </dl>
          </fieldset>
          <div style='text-align:right'>
            <button type='submit' name='btnSaveChanges' class='btn btn-md btn-success'><span class='glyphicon glyphicon-floppy-disk'></span> Save Changes</button>
          </div>
        </form>";
      }
    }else echo "<div class='alert alert-danger' role='alert'>
      <strong><span class='glyphicon glyphicon-warning-sign'></span> Warning!!!</strong> No User selected.
    </div>";
    include_once "pages/footer.php";
  }else header("Location:/../Logout");
?>
