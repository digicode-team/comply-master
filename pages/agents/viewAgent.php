<?php
  session_name("ComplyMaster");
  session_start();
  if((isset($_SESSION["tout"]))&&($_SESSION["tout"]>time())) {
    $pTime = time();
    $expTime = $pTime + 600;
    $_SESSION["tout"] = $expTime;
    header ("Expires: ".gmdate("D, d M Y H:i:s", time())." GMT");
    header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header ("Cache-Control: no-cache, must-revalidate");
    header ("Pragma: no-cache");
    $title = 'View Agent';
    include_once "pages/header.php";
    require_once "config.php";
    require_once "DAO/users.php";
    $db = new Database();
    $users = new Users($db);
    echo "<h2>Agent Details</h2>";
    if(isset($args[2])){
      $parameters = array(
        "uniqId=" => $args[2]
      );
      $strUsers = $users->fetchUsers($parameters);
      if(sizeof($strUsers)==0){
        echo "<div class='alert alert-danger' role='alert'>
          <strong><span class='glyphicon glyphicon-warning-sign'></span> Warning!!!</strong> The Agent does not exists.
        </div>";
      }else{
        $URow = $strUsers[0];
        echo "<fieldset style='font-weight:bold'>
          <legend>General Details</legend>
          <dl class='dl-horizontal'>
            <dt>Username :</dt><dd>$URow[username]</dd>
            <dt>Company Name :</dt><dd>$URow[company]</dd>
            <dt>Address :</dt><dd>$URow[address]</dd>
            <dt>Post Code :</dt><dd>$URow[postCode]</dd>
            <dt>City :</dt><dd>$URow[city]</dd>
            <dt>Country :</dt><dd>$URow[country]</dd>
            <dt>TIC :</dt><dd>$URow[tic]</dd>
          </dl>
        </fieldset>
        <fieldset style='font-weight:bold'>
          <legend>Contact Details</legend>
          <dl class='dl-horizontal'>
            <dt>Name :</dt><dd>$URow[name]</dd>
            <dt>Surname :</dt><dd>$URow[surname]</dd>
            <dt>Telephone :</dt><dd>$URow[telephone]</dd>
            <dt>Cellphone :</dt><dd>$URow[cellphone]</dd>
            <dt>Email :</dt><dd>$URow[email]</dd>
          </dl>
        </fieldset>
        <fieldset style='font-weight:bold'>
          <legend>Billing Details</legend>
          <dl class='dl-horizontal'>
            <dt>Company Name :</dt><dd>$URow[billingCompany]</dd>
            <dt>Address :</dt><dd>$URow[billingAddress]</dd>
            <dt>Post Code :</dt><dd>$URow[billingPostCode]</dd>
            <dt>City :</dt><dd>$URow[billingCity]</dd>
            <dt>Country :</dt><dd>$URow[billingCountry]</dd>
            <dt>VAT :</dt><dd>$URow[billingVAT]</dd>
          </dl>
        </fieldset>
        <fieldset style='font-weight:bold'>
          <legend>Mail Server Details</legend>
          <dl class='dl-horizontal'>
            <dt>Username :</dt><dd>$URow[emailUsername]</dd>
            <dt>Password :</dt><dd>$URow[emailPassword]</dd>
            <dt>Outgoing Server :</dt><dd>$URow[emailServer]</dd>
            <dt>Port :</dt><dd>$URow[emailPort]</dd>
          </dl>
        </fieldset>";
      }
    }else echo "<div class='alert alert-danger' role='alert'>
      <strong><span class='glyphicon glyphicon-warning-sign'></span> Warning!!!</strong> No Agent selected.
    </div>";
    include_once "pages/footer.php";
  }else header("Location:/../Logout");
?>
