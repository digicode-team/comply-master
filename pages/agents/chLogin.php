<?php
	session_name("ComplyMaster");
	session_start();
	if((isset($_SESSION["tout"]))&&($_SESSION["tout"]>time())) {
		$pTime = time();
		$expTime = $pTime + 600;
		$_SESSION["tout"] = $expTime;
		header ("Expires: ".gmdate("D, d M Y H:i:s", time())." GMT");
		header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
		require_once "config.php";
		require_once "DAO/users.php";
		$db = new Database();
		$users = new Users($db);
		//Page

		if(isset($_POST["user"])){
			$parameters = array(
				"uniqId=" => $_POST["user"]
			);
			$userExists=$users->fetchUser($parameters);
			if(sizeof($userExists)!=0){
				if($_SESSION['userAccount']==1){
					if($userExists['login']==0) $login=1; else $login=0;
					$values = array(
						"login" => $login
					);
					$parameters = array(
						"uniqId=" => $_POST["user"]
					);
					$statement=$users->updateUser($values,$parameters);
					$message = "1";
				}else{
					$message = "You are not allowed for this action";
				}
			}else{
				$message = "User does not exists";
			}
			echo $message;
		}
	}
?>
