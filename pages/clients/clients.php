<?php
  session_name("ComplyMaster");
  session_start();
  if((isset($_SESSION["tout"]))&&($_SESSION["tout"]>time())) {
    $pTime = time();
    $expTime = $pTime + 600;
    $_SESSION["tout"] = $expTime;
    header ("Expires: ".gmdate("D, d M Y H:i:s", time())." GMT");
    header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header ("Cache-Control: no-cache, must-revalidate");
    header ("Pragma: no-cache");
    $title = 'Clients';
    include_once "pages/header.php";
    require_once "config.php";
    require_once "DAO/clients.php";
    $db = new Database();
    $clients = new Clients($db);
    if(isset($_POST['btnAddClient'])) {
      $insertParams = array('agent' => $_SESSION['userId']);
      if(strlen($_POST['txtName'])>0) $insertParams['name'] = str_replace('"',"`",str_replace("'","`",$_POST['txtName']));
      if(strlen($_POST['txtSurname'])>0) $insertParams['surname'] = str_replace('"',"`",str_replace("'","`",$_POST['txtSurname']));
      if(strlen($_POST['txtCompanyName'])>0) $insertParams['company'] = str_replace('"',"`",str_replace("'","`",$_POST['txtCompanyName']));
      if(strlen($_POST['txtAddress'])>0) $insertParams['address'] = str_replace('"',"`",str_replace("'","`",$_POST['txtAddress']));
      if(strlen($_POST['txtPostCode'])>0) $insertParams['postCode'] = str_replace('"',"`",str_replace("'","`",$_POST['txtPostCode']));
      if(strlen($_POST['txtCity'])>0) $insertParams['city'] = str_replace('"',"`",str_replace("'","`",$_POST['txtCity']));
      if(strlen($_POST['txtCountry'])>0) $insertParams['country'] = str_replace('"',"`",str_replace("'","`",$_POST['txtCountry']));
      if(strlen($_POST['txtTIC'])>0) $insertParams['tic'] = str_replace('"',"`",str_replace("'","`",$_POST['txtTIC']));
      if(strlen($_POST['txtTel'])>0) $insertParams['tel'] = str_replace('"',"`",str_replace("'","`",$_POST['txtTel']));
      if(strlen($_POST['txtMob'])>0) $insertParams['mob'] = str_replace('"',"`",str_replace("'","`",$_POST['txtMob']));
      if(strlen($_POST['txtEmail'])>0) $insertParams['email'] = str_replace('"',"`",str_replace("'","`",$_POST['txtEmail']));
			$clientsLastId=$clients->insertClient($insertParams);
			if($clientsLastId>0){
        echo "<div class='alert alert-success' role='alert'>
          <strong><span class='glyphicon glyphicon-ok'></span> Success!!!</strong> Record has been added successfully.
          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>
          </button>
        </div>";
      }else{
        echo "<div class='alert alert-danger' role='alert'>
          <strong><span class='glyphicon glyphicon-warning-sign'></span> Warning!!!</strong> An error has occurred. Please try again later.
          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>
          </button>
        </div>";
      }
    }
    $clientsParameters = array("agent=" => $_SESSION['userId']);
		$strClients=$clients->fetchClients($clientsParameters);
    echo "<form action='/..$_SERVER[REQUEST_URI]' method='post'>
			<div class='modal fade' id='addClient' tabindex='-1' role='dialog' aria-labelledby='formTitle' aria-hidden='true'>
				<div class='modal-dialog'>
					<div class='modal-content'>
						<div class='modal-header'>
							<button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span></button>
							<h4 class='modal-title' id='formTitle'><i class='fa fa-user-plus'></i> New Client</h4>
						</div>
						<div class='modal-body'>
              <fieldset style='font-weight:bold'>
                <legend>General Details</legend>
                <div class='row'>
                  <div class='col-sm-12'>Company Name<input type='text' name='txtCompanyName' class='form-control'/></div>
                </div>
                <div class='row'>
                  <div class='col-sm-12'>Address<input type='text' name='txtAddress' class='form-control'/></div>
                </div>
                <div class='row'>
                  <div class='col-md-4'>Post Code<input type='number' name='txtPostCode' class='form-control'/></div>
                  <div class='col-md-4'>City<input type='text' name='txtCity' class='form-control'/></div>
                  <div class='col-md-4'>Country<input type='text' name='txtCountry' class='form-control'/></div>
                </div>
                <div class='row'>
                  <div class='col-sm-12'>TIC<input type='text' name='txtTIC' class='form-control'/></div>
                </div>
              </fieldset>
              <fieldset style='font-weight:bold'>
                <legend>Contact Details</legend>
                <div class='row'>
                  <div class='col-md-6'>Name <span class='required'>*</span><input type='text' name='txtName' class='form-control' required/></div>
                  <div class='col-md-6'>Surname <span class='required'>*</span><input type='text' name='txtSurname' class='form-control' required/></div>
                </div>
                <div class='row'>
                  <div class='col-md-6'>Telephone<input type='text' name='txtTel' class='form-control'/></div>
                  <div class='col-md-6'>Cellphone<input type='text' name='txtMob' class='form-control'/></div>
                </div>
                <div class='row'>
                  <div class='col-sm-12'>Email <span class='required'>*</span><input type='email' name='txtEmail' class='form-control' required/></div>
                </div>
              </fieldset>
            </div>
						<div class='modal-footer'>
							<div align='left'><span class='required'>Fields with (*) are required.</span></div>
							<button type='button' class='shadow-z-2 btn btn-default' data-dismiss='modal'><span class='glyphicon glyphicon-log-out'></span> Close</button>
							<button type='submit' name='btnAddClient' class='shadow-z-2 btn btn-success'><span class='glyphicon glyphicon-floppy-disk'></span> Add</button>
						</div>
					</div>
				</div>
			</div>
		</form>
    <div class='row'>
      <div class='col-xs-12'>
        <div class='x_panel shadow-z-2'>
          <div class='x_title'>
            <h2>Clients</h2><a href='#' data-toggle='modal' data-target='#addClient' data-backdrop='static' class='btn btn-primary' style='margin-left: 20px;'><i class='fa fa-user-plus'></i> Add New Client</a>
            <ul class='nav navbar-right panel_toolbox'>
              <li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a></li>
              <li><a class='close-link'><i class='fa fa-close'></i></a></li>
            </ul>
            <div class='clearfix'></div>
          </div>
          <div class='x_content'>";
          if(sizeof($strClients)==0){
            echo "<div class='alert alert-danger' role='alert'>No data found.</div>";
          }else{
            echo "<table class='table table-striped table-hover'>
              <thead>
                <tr>
                  <th>Company</th>
                  <th>Contact Name</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>";
              for($i=0;$i<sizeof($strClients);$i++){
                $CRow = $strClients[$i];
                echo "<tr>
                  <td>$CRow[company]</td>
                  <td>$CRow[name] $CRow[surname]</td>
                  <td>
                    <a href='/../ViewClient/$CRow[uniqId]'><i class='fa fa-eye'></i></a>
                    <a href='/../EditClient/$CRow[uniqId]'><i class='fa fa-pencil'></i></a>
                  </td>
                </tr>";
              }
              echo "</tbody>
            </table>";
          }
          echo "</div>
          </div>
        </div>
      </div>
      <script>
        function checkBilling() {
					if(document.getElementById('chkBilling').checked){
            $(document).ready(function(){ $('#divBillingCompanyName').hide(1000); });
            $(document).ready(function(){ $('#divBillingAddress').hide(1000); });
            $(document).ready(function(){ $('#divBillingAddress2').hide(1000); });
            $(document).ready(function(){ $('#divBillingVAT').hide(1000); });
					}else{
            $(document).ready(function(){ $('#divBillingCompanyName').show(1000); });
            $(document).ready(function(){ $('#divBillingAddress').show(1000); });
            $(document).ready(function(){ $('#divBillingAddress2').show(1000); });
            $(document).ready(function(){ $('#divBillingVAT').show(1000); });
					}
				}
      </script>";

    include_once "pages/footer.php";
  }else header("Location:/../Logout");
?>
