<?php
  session_name("ComplyMaster");
  session_start();
  if((isset($_SESSION["tout"]))&&($_SESSION["tout"]>time())) {
    $pTime = time();
    $expTime = $pTime + 600;
    $_SESSION["tout"] = $expTime;
    header ("Expires: ".gmdate("D, d M Y H:i:s", time())." GMT");
    header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header ("Cache-Control: no-cache, must-revalidate");
    header ("Pragma: no-cache");
    $title = 'Edit Client';
    include_once "pages/header.php";
    require_once "config.php";
    require_once "DAO/clients.php";
    $db = new Database();
    $clients = new Clients($db);
    echo "<h2>Client Details</h2>";
    if(isset($args[2])){
      $parameters = array(
        "uniqId=" => $args[2]
      );
      $strClients = $clients->fetchClients($parameters);
      if(sizeof($strClients)==0){
        echo "<div class='alert alert-danger' role='alert'>
          <strong><span class='glyphicon glyphicon-warning-sign'></span> Warning!!!</strong> The Client does not exists.
        </div>";
      }else{
        if(isset($_POST['btnSaveChanges'])){
          $updateValues['name'] = str_replace('"',"`",str_replace("'","`",$_POST['txtName']));
          $updateValues['surname'] = str_replace('"',"`",str_replace("'","`",$_POST['txtSurname']));
          $updateValues['company'] = str_replace('"',"`",str_replace("'","`",$_POST['txtCompanyName']));
          $updateValues['address'] = str_replace('"',"`",str_replace("'","`",$_POST['txtAddress']));
          $updateValues['postCode'] = str_replace('"',"`",str_replace("'","`",$_POST['txtPostCode']));
          $updateValues['city'] = str_replace('"',"`",str_replace("'","`",$_POST['txtCity']));
          $updateValues['country'] = str_replace('"',"`",str_replace("'","`",$_POST['txtCountry']));
          $updateValues['tic'] = str_replace('"',"`",str_replace("'","`",$_POST['txtTIC']));
          $updateValues['tel'] = str_replace('"',"`",str_replace("'","`",$_POST['txtTel']));
          $updateValues['mob'] = str_replace('"',"`",str_replace("'","`",$_POST['txtMob']));
          $updateValues['email'] = str_replace('"',"`",str_replace("'","`",$_POST['txtEmail']));
          $updateParams = array(
            "uniqId=" => $args[2]
          );
          $statement=$clients->updateClient($updateValues,$updateParams);
          if($statement==1){
            echo "<div class='alert alert-success' role='alert'>
              <strong><span class='glyphicon glyphicon-ok'></span> Success!!!</strong> Record has been updated successfully.
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button>
            </div>";
          }else{
            echo "<div class='alert alert-danger' role='alert'>
              <strong><span class='glyphicon glyphicon-warning-sign'></span> Warning!!!</strong> An error has occurred. Please try again later.
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button>
            </div>";
          }
        }
        $strClients = $clients->fetchClients($parameters);
        $CRow = $strClients[0];
        echo "<form action='/..$_SERVER[REQUEST_URI]' method='post'>
          <fieldset style='font-weight:bold'>
            <legend>Contact Details</legend>
            <legend>General Details</legend>
            <dl class='dl-horizontal'>
              <dt>Company Name :</dt><dd><input type='text' name='txtCompanyName' class='form-control' value='$CRow[company]' /></dd>
              <dt>Address :</dt><dd><input type='text' name='txtAddress' class='form-control' value='$CRow[address]' /></dd>
              <dt>Post Code :</dt><dd><input type='number' name='txtPostCode' class='form-control' value='$CRow[postCode]' /></dd>
              <dt>City :</dt><dd><input type='text' name='txtCity' class='form-control' value='$CRow[city]' /></dd>
              <dt>Country :</dt><dd><input type='text' name='txtCountry' class='form-control' value='$CRow[country]' /></dd>
              <dt>TIC :</dt><dd><input type='text' name='txtTIC' class='form-control' value='$CRow[tic]' /></dd>
            </dl>
          </fieldset>
          <fieldset style='font-weight:bold'>
            <legend>Contact Details</legend>
            <dl class='dl-horizontal'>
              <dt>Name <span class='required'>*</span> :</dt><dd><input type='text' name='txtName' class='form-control' value='$CRow[name]' required /></dd>
              <dt>Surname <span class='required'>*</span> :</dt><dd><input type='text' name='txtSurname' class='form-control' value='$CRow[surname]' required /></dd>
              <dt>Telephone :</dt><dd><input type='text' name='txtTel' class='form-control' value='$CRow[telephone]' /></dd>
              <dt>Cellphone :</dt><dd><input type='text' name='txtMob' class='form-control' value='$CRow[cellphone]' /></dd>
              <dt>Email <span class='required'>*</span> :</dt><dd><input type='text' name='txtEmail' class='form-control' value='$CRow[email]' required /></dd>
            </dl>
          </fieldset>
          <div style='text-align:right'>
            <button type='submit' name='btnSaveChanges' class='btn btn-md btn-success'><span class='glyphicon glyphicon-floppy-disk'></span> Save Changes</button>
          </div>
        </form>";
      }
    }else echo "<div class='alert alert-danger' role='alert'>
      <strong><span class='glyphicon glyphicon-warning-sign'></span> Warning!!!</strong> No Client selected.
    </div>";
    include_once "pages/footer.php";
  }else header("Location:/../Logout");
?>
