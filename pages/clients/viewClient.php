<?php
  session_name("ComplyMaster");
  session_start();
  if((isset($_SESSION["tout"]))&&($_SESSION["tout"]>time())) {
    $pTime = time();
    $expTime = $pTime + 600;
    $_SESSION["tout"] = $expTime;
    header ("Expires: ".gmdate("D, d M Y H:i:s", time())." GMT");
    header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header ("Cache-Control: no-cache, must-revalidate");
    header ("Pragma: no-cache");
    $title = 'View Client';
    include_once "pages/header.php";
    require_once "config.php";
    require_once "DAO/clients.php";
    $db = new Database();
    $clients = new Clients($db);
    echo "<h2>Client Details</h2>";
    if(isset($args[2])){
      $parameters = array(
        "uniqId=" => $args[2]
      );
      $strClients = $clients->fetchClients($parameters);
      if(sizeof($strClients)==0){
        echo "<div class='alert alert-danger' role='alert'>
          <strong><span class='glyphicon glyphicon-warning-sign'></span> Warning!!!</strong> The Client does not exists.
        </div>";
      }else{
        $CRow = $strClients[0];
        echo "<fieldset style='font-weight:bold'>
          <legend>General Details</legend>
          <dl class='dl-horizontal'>
            <dt>Company Name :</dt><dd>$CRow[company]</dd>
            <dt>Address :</dt><dd>$CRow[address]</dd>
            <dt>Post Code :</dt><dd>$CRow[postCode]</dd>
            <dt>City :</dt><dd>$CRow[city]</dd>
            <dt>Country :</dt><dd>$CRow[country]</dd>
            <dt>TIC :</dt><dd>$CRow[tic]</dd>
          </dl>
        </fieldset>
        <fieldset style='font-weight:bold'>
          <legend>Contact Details</legend>
          <dl class='dl-horizontal'>
            <dt>Name :</dt><dd>$CRow[name]</dd>
            <dt>Surname :</dt><dd>$CRow[surname]</dd>
            <dt>Telephone :</dt><dd>$CRow[telephone]</dd>
            <dt>Cellphone :</dt><dd>$CRow[cellphone]</dd>
            <dt>Email :</dt><dd>$CRow[email]</dd>
          </dl>
        </fieldset>";
      }
    }else echo "<div class='alert alert-danger' role='alert'>
      <strong><span class='glyphicon glyphicon-warning-sign'></span> Warning!!!</strong> No Client selected.
    </div>";
    include_once "pages/footer.php";
  }else header("Location:/../Logout");
?>
