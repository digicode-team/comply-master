<?php
  class Users {
    private $db;
    public function __construct(Database $db) {
        $this->db = $db;
    }
    public function fetchUsers($parameters) {
      $selectKeys = [];
      $selectValues = [];
      $where = "";
      if(sizeof($parameters)>0){
        foreach ($parameters as $key => $value) {
          array_push($selectKeys,"$key?");
          array_push($selectValues,$value);
        }
        $conditions = implode(" AND ",$selectKeys);
        $where = " WHERE $conditions";
      }
      $query = "SELECT * FROM users $where";
      return $this->db->fetchAll($query,$selectValues);
    }
    public function fetchUser($parameters) {
      $selectKeys = [];
      $selectValues = [];
      $where = "";
      if(sizeof($parameters)>0){
        foreach ($parameters as $key => $value) {
          array_push($selectKeys,"$key?");
          array_push($selectValues,$value);
        }
        $conditions = implode(" AND ",$selectKeys);
        $where = " WHERE $conditions";
      }
      $query = "SELECT * FROM users$where";
      return $this->db->fetchOne($query, $selectValues);
    }
    public function fetchUserLogin($parameters) {
      $selectKeys = [];
      $selectValues = [];
      $where = "";
      if(sizeof($parameters)>0){
        foreach ($parameters as $key => $value) {
          array_push($selectKeys,"$key?");
          array_push($selectValues,$value);
        }
        $conditions = implode(" AND ",$selectKeys);
        $where = " WHERE $conditions";
      }
      $query = "SELECT * FROM users$where";
      return $this->db->fetchOne($query, $selectValues);
    }
    public function fetchAuth($username,$password) {
        $query = "SELECT uUniqId as id,uCreatedDate as created_date,uFullname as fullname,
          uTel as telephone,uEmail as email,uLang as language,uRole as role_id,uAccount as account_id
          FROM users WHERE uUsername=? AND uPassword=?";
        return $this->db->fetchAuthenticate($query,$username,$password);
    }
    public function fetchAccounts($parameters) {
      $selectKeys = [];
      $selectValues = [];
      $where = "";
      if(sizeof($parameters)>0){
        foreach ($parameters as $key => $value) {
          array_push($selectKeys,"$key?");
          array_push($selectValues,$value);
        }
        $conditions = implode(" AND ",$selectKeys);
        $where = " WHERE $conditions";
      }
      $query = "SELECT aId AS id, aName AS name FROM accounts$where ORDER BY aName";
      return $this->db->fetchAll($query,$selectValues);
    }
    public function updateUser($values,$parameters){
      $vKeys = [];
      $vValues = [];
      if(sizeof($values)>0){
        foreach ($values as $key => $value) {
          array_push($vKeys,"$key=?");
          array_push($vValues,$value);
        }
        $fields = implode(",",$vKeys);
      }
      $selectKeys = [];
      $selectValues = [];
      if(sizeof($parameters)>0){
        foreach ($parameters as $key => $value) {
          array_push($selectKeys,"$key?");
          array_push($selectValues,$value);
        }
        $conditions = implode(" AND ",$selectKeys);
      }
      $allValues = array_merge($vValues, $selectValues);
      $query = "UPDATE users SET $fields WHERE $conditions";
      return $this->db->updateOne($query,$allValues);
    }
    public function insertUser($parameters){
      $selectKeys = [];
      $selectKeysNo = [];
      $selectValues = [];
      if(sizeof($parameters)>0){
        foreach ($parameters as $key => $value) {
          array_push($selectKeys,$key);
          array_push($selectKeysNo,"?");
          array_push($selectValues,$value);
        }
        $fields = implode(",",$selectKeys);
        $values = implode(",",$selectKeysNo);
        $query = "INSERT INTO users (uniqId,account,$fields) SELECT UUID(),1,$values";
        return $this->db->insertOne($query,$selectValues);
      }else return 0;
    }
    public function insertAgent($parameters){
      $selectKeys = [];
      $selectKeysNo = [];
      $selectValues = [];
      if(sizeof($parameters)>0){
        foreach ($parameters as $key => $value) {
          array_push($selectKeys,$key);
          array_push($selectKeysNo,"?");
          array_push($selectValues,$value);
        }
        $fields = implode(",",$selectKeys);
        $values = implode(",",$selectKeysNo);
        $query = "INSERT INTO users (uniqId,privateKey,account,$fields) SELECT UUID(),UUID(),2,$values";
        return $this->db->insertOne($query,$selectValues);
      }else return 0;
    }
  }
?>
