<?php
  class Clients {
    private $db;
    public function __construct(Database $db) {
        $this->db = $db;
    }
    public function fetchClients($parameters) {
      $selectKeys = [];
      $selectValues = [];
      $where = "";
      if(sizeof($parameters)>0){
        foreach ($parameters as $key => $value) {
          array_push($selectKeys,"$key?");
          array_push($selectValues,$value);
        }
        $conditions = implode(" AND ",$selectKeys);
        $where = " WHERE $conditions";
      }
      $query = "SELECT * FROM clients $where";
      return $this->db->fetchAll($query,$selectValues);
    }
    public function fetchClient($parameters) {
      $selectKeys = [];
      $selectValues = [];
      $where = "";
      if(sizeof($parameters)>0){
        foreach ($parameters as $key => $value) {
          array_push($selectKeys,"$key?");
          array_push($selectValues,$value);
        }
        $conditions = implode(" AND ",$selectKeys);
        $where = " WHERE $conditions";
      }
      $query = "SELECT * FROM clients$where";
      return $this->db->fetchOne($query, $selectValues);
    }
    public function updateClient($values,$parameters){
      $vKeys = [];
      $vValues = [];
      if(sizeof($values)>0){
        foreach ($values as $key => $value) {
          array_push($vKeys,"$key=?");
          array_push($vValues,$value);
        }
        $fields = implode(",",$vKeys);
      }
      $selectKeys = [];
      $selectValues = [];
      if(sizeof($parameters)>0){
        foreach ($parameters as $key => $value) {
          array_push($selectKeys,"$key?");
          array_push($selectValues,$value);
        }
        $conditions = implode(" AND ",$selectKeys);
      }
      $allValues = array_merge($vValues, $selectValues);
      $query = "UPDATE clients SET $fields WHERE $conditions";
      return $this->db->updateOne($query,$allValues);
    }
    public function insertClient($parameters){
      $selectKeys = [];
      $selectKeysNo = [];
      $selectValues = [];
      if(sizeof($parameters)>0){
        foreach ($parameters as $key => $value) {
          array_push($selectKeys,$key);
          array_push($selectKeysNo,"?");
          array_push($selectValues,$value);
        }
        $fields = implode(",",$selectKeys);
        $values = implode(",",$selectKeysNo);
        $query = "INSERT INTO clients (uniqId,$fields) SELECT UUID(),$values";
        return $this->db->insertOne($query,$selectValues);
      }else return 0;
    }
  }
?>
