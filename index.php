﻿<?php
	$args = explode("/",$_SERVER['REQUEST_URI']);
	switch ($args[1]) {
		case "Home":
			require_once "pages/home.php";
			break;
		case "Dashboard":
			require_once "pages/dashboard.php";
			break;
		case "Users":
			require_once "pages/users/users.php";
			break;
		case "ViewUser":
			require_once "pages/users/viewUser.php";
			break;
		case "EditUser":
			require_once "pages/users/editUser.php";
			break;
		case "ChangeLoginUser":
			require_once "pages/users/chLogin.php";
			break;
		case "Agents":
			require_once "pages/agents/agents.php";
			break;
		case "ViewAgent":
			require_once "pages/agents/viewAgent.php";
			break;
		case "EditAgent":
			require_once "pages/agents/editAgent.php";
			break;
		case "ChangeLoginAgent":
			require_once "pages/agents/chLogin.php";
			break;
		case "Taxations":
			require_once "pages/taxations.php";
			break;
		case "Clients":
			require_once "pages/clients/clients.php";
			break;
		case "ViewClient":
			require_once "pages/clients/viewClient.php";
			break;
		case "EditClient":
			require_once "pages/clients/editClient.php";
			break;
		case "Logout":
			require_once "pages/logout.php";
			break;
		default:
			header("Location:/../Home");
	}
?>
